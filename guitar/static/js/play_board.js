// colors[0]: index finger; colors[0]: middle finger; colors[0]: ring finger; colors[0]: little thumb;
const colors = ["#7bdff2", "#fcf6bd", "#f7d6e0", "#b2f7ef"]
const errorColor = "#ff0000"
const correctColor = "#38b000"


const correct = [[0,0], [3,2], [3,1], [3,0], [2,3], [5,1]]

// "Guitar board"
const table = $("table tr");


// if a string is pushed, then lighten it
function changeColor(rowIndex, colIndex, color) {
  return table
    .eq(rowIndex)
    .find("td")
    .eq(colIndex)
    .find("button")
    .css("background-color", color);
}


function setColorBack(rowIndex, colIndex) {
  return table
    .eq(rowIndex)
    .find("td")
    .eq(colIndex)
    .find("button")
    .css("background-color", "#efefef");
}


function checkIfCorrect(press, row, col, colorNum) {
  if(row === correct[press][0] && col === correct[press][1]) {
    changeColor(row, col, colors[colorNum]);
    return true
  }else {
    changeColor(row, col, errorColor);
    changeColor(correct[press][0], correct[press][1], correctColor)
    return false
  }
}


function playColor() {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "http://127.0.0.1:8000/data", true);

  xhr.onload = function () {
    if (this.status === 200) {
      const sounds = JSON.parse(this.responseText);
      let preRow
      let preCol
      let error
      let accuracy

      for (let i = 0; i < sounds.length; i++) {
        // let time = 100 * Math.ceil(Math.random()*10)  // set random time to simulate real playing process
        setTimeout(() => {
          let data = sounds[i].finger_data; // sound per time -- 6x4 array

          for (let row = 0; row < 6; row++) {
            for(let col = 0; col < 4; col ++){
              if(data[row][col] !== 0){  // this string will being pushed
                if(i === 0) {
                  preRow = row
                  preCol = col
                }
                let colorNum = data[row][col] - 1
                if(checkIfCorrect(i, row, col, colorNum) === false) {
                  error += 1
                }

                if(i !== 0) {
                  setColorBack(preRow, preCol);
                  preRow = row
                  preCol = col
                }
              }
            }
          }
		},1000 * i)  // press once per second
      }
      accuracy = error / sounds.length
      console.log(accuracy)
    }
  };
  xhr.send();
}


// Select "start-play" button and and "click" event listener
$("#start-play").on("click", playColor);


