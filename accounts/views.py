from django.shortcuts import redirect, render
from django.contrib import messages, auth
from django.contrib.auth.models import User


def register(request):
    if request.method == 'POST':
        # messages.error(request, "Testing error message")
        # Register User 
        first_name = request.POST["first_name"]
        last_name = request.POST["last_name"]
        username = request.POST["username"]
        password = request.POST["password"]
        email = request.POST["email"]
        password2 = request.POST["password2"]

        # Check is password match 
        if password == password2:
            # Check username
            if User.objects.filter(username=username).exists():
                messages.error(request, 'Sorry, that username is being used')
                return redirect('register')
            else:
                if User.objects.filter(email=email).exists():
                    messages.error(request, 'Sorry, that email is being used ')
                    return redirect('register')
                else: 
                    # everything is ok 
                    user = User.objects.create_user(username=username, password=password, email=email, first_name=first_name, last_name=last_name)
                    # Login after register 
                    # auth.login(request, user)
                    # messages.success(request, 'You are now log in')
                    # return redirect('index')

                    user.save()
                    messages.success(request, 'You are now registered and can log in')
                    return redirect('login')
        else:
            messages.error(request, 'Passwords do not match')
            return redirect('register')
    else:
        return render(request, 'accounts/register.html')


def login(request):
    if request.method == 'POST':
        # messages.error(request, "Testing error message")
        # Login User
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None: # the user is found in the database
            auth.login(request, user)
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid credentials')
            return redirect('login')
    else:
        return render(request, 'accounts/login.html')


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('index')
    return redirect('index')


def dashboard(request):

    return render(request, 'accounts/dashboard.html')
