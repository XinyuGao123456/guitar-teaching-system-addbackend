from django.shortcuts import render
from django.http import HttpResponse
from .models import Student


def index(request):
    return render(request, 'pages/index.html')


def about(request):
    return render(request, 'pages/about.html')


def students(request):
    students = Student.objects.all()
    context = {
        'students': students
    }
    return render(request, 'pages/students.html', context)


def student(request, student_id):
    return render(request, 'pages/student.html')


def courses(request):
    return render(request, 'pages/courses.html')


