from django.urls import path
from . import views
from .custom_api_views import TestView

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('students/', views.students, name='students'),
    path('courses/', views.courses, name='courses'),
    path('<int:student_id>', views.student, name='student'),
    path('data/', TestView.as_view(), name='test'),

]