from django.db import models


class Student(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField()
    description = models.TextField(blank=True)  # about which part the student wants to learn
    email = models.CharField(max_length=40)
    level = models.CharField(max_length=50, blank=True)
    # level = models.IntegerField()

    def __str__(self):
        return self.name


