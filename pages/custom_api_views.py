from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response


class TestView(APIView):
    def get(self, request, *args, **kwargs):
        data = [
            {
                "finger_data": [
                    [1, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            },
            {
                "finger_data": [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 2, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            },
            {
                "finger_data": [
                    [0, 0, 2, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 3, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            },

            {
                "finger_data": [
                    [0, 0, 0, 4],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [1, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            },

            {
                "finger_data": [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 2],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0]
                ]
            },

            {
                "finger_data": [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 4, 0, 0]
                ]
            }
        ]
        return Response(data)